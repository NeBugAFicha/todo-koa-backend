<<<<<<< HEAD
# todo-koa-backend



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/NeBugAFicha/todo-koa-backend.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/NeBugAFicha/todo-koa-backend/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
=======
## Как запустить приложение

- Установить Docker;
- Зайти в папку приложение "todo-koa-backend"
- В терминале прописать docker compose -f render.yaml up --build -d
- После того как докер создаст два контейнера, прописать в терминале: 
  docker exec nodejs npm run migrate
- Все, готово, приложение крутится на localhost:8000, данные для входа в базу есть в файле docker-compose.yml. В базе данных уже находятся два юзера "EgorDeveloper" и "AnotherUser", у обоих пользователей пароль "12341234"
## Возможность протестировать backend так же можно с помощью урла https://todo-koa-backend.onrender.com
Предварительно туда уже были два юзера "EgorDeveloper" и "AnotherUser", у обоих пользователей пароль "12341234"
## API приложения

### ВСЕ РУТЫ КРОМЕ /registration, /login должны использоваться вместе с параметром token в headers

- POST '/registration' Регистрация пользователя
  Параметры:  
  1) body 
        - login (string, минимум 1 символ, обязательный параметр);
        - password (string, минимум 8 символов, , обязательный параметр);
  
  Возвращает токен для запросов в другие руты кроме /registration, /login:
    - token (string)

- POST '/login'  Логин
  Параметры:  
  1) body 
        - login (string, минимум 1 символ, обязательный параметр);
        - password (string, минимум 8 символов, обязательный параметр);

  Возвращает токен для запросов в другие руты кроме /registration, /login: 
    - message (string);
    - token (string);

- GET '/task_list' Лист списков задач
  Параметры:  
  1) query 
        - my_list (boolean, обязательный параметр);
  
  Возвращает лист списоков задач: my_list = true -> только собственный лист, my_list = false -> лист из списков задач, в которых есть хоть какие-либо права

- GET '/task_list/:id' Конкретный список задач
  Параметры:  
  1) params 
        - id (number, положительное число, обязательный параметр);

  Возвращает список задач, в котором есть хоть какие-либо права вместе с доступными правами для этого списка, а если есть право на создателя или чтение, то возвращаются еще и задания этого списка, 

- POST '/task_list' Создание списка задач
  Параметры:  
  1) body 
        - name (string, минимум 1 символ);
      
  Возвращает только что созданный список задач

- '/task_list/:id' Удаление списка задач (с каскадным удалением ВСЕХ его задач)
  Параметры:  
  1) params 
        - id (number, положительное число, обязательный параметр);
      
  Возвращает только что удаленный список задач

- GET '/task_list/list' Конкретный лист списков задач (свой или в котором есть права на чтение)
  Параметры:  
  1) query 
        - list_id (number, положительное число, необязательный параметр, list_id это идентификатор юзера, чьи списки задач необходимо получить);
      
  Возвращает свой лист списков задач либо лист конкретного юзера из списоков задач, в которых есть право на чтение

- GET '/task_list/:task_list_id/grant' Выдать/отнять права на какое либо действие для задач внутри  конкретного списка другому пользователю - только в своих списках
  Параметры:  
  1) query 
        - grant (string, 4 валидных значения 'Create', 'Read', 'Update', 'Delete', (нечувствителен к регистру, обязательный параметр);
        - user_id (number, положительное число, обязательный параметр);
        - take_off (boolean, необязательный параметр);
    
  2) params 
        - task_list_id (number, положительное число, обязательный параметр);
      
  Если take_off= false | undefined - дать права, если true - отнять

- GET '/task_list/:task_list_id/task/:id' - Запросить задачу в списке - в своем или в списке в котором есть права на чтение
  Параметры:  
  1) params 
        - task_list_id (number, положительное число, обязательный параметр);
        - id (number, положительное число, обязательный параметр);
      
  Возвращает конкретную задачу

- POST '/task_list/:task_list_id/task/:id' - Запросить задачу в списке - в своем или в списке в котором есть права на создание
  Параметры:  
  1) params 
        - task_list_id (number, положительное число, обязательный параметр);
      
  2) body 
        - name (string, минимум 1 символ);
      
  Возвращает созданную задачу

- DELETE '/task_list/:task_list_id/task/:id' - Запросить задачу в списке - в своем или в списке в котором есть права на удаление
  Параметры:  
  1) params 
        - task_list_id (number, положительное число, обязательный параметр),
        - id (number, положительное число, обязательный параметр),
      
  Возвращает удаленную задачу

- PUT '/task_list/:task_list_id/task/:id' - Запросить задачу в списке - в своем или в списке в котором есть права на изменение
  Параметры:  
  1) params 
        - task_list_id (number, положительное число, обязательный параметр);
        - id (number, положительное число, обязательный параметр);
      
  2) body 
        - name (string, минимум 1 символ);
      
  Возвращает обновленную задачу


  
>>>>>>> commit
